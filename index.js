const express = require('express');
const admin = require('firebase-admin');
const serviceAccount = require('./serviceAccountKey.json');
require('dotenv').config();

const accountSid = process.env.TWILIO_ACCOUNT_SID;
console.log(accountSid);
const authToken =  process.env.TWILIO_AUTH_TOKEN;

const client = require('twilio')(accountSid, authToken);

const app = express();

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://myhome-a7b49.firebaseio.com"
  });

const db = admin.database();

let ref = db.ref("automation/dustbin");

ref.on("value", function(snapshot) {
    let sensorData = snapshot.val();

    if(sensorData <= 20){
      console.log("Waste bin is full");
        client.messages.create({
          to: process.env.TO_NUMBER,
          from: process.env.FROM_NUMBER,
          body: "Waste bin is full"
        })
        .then((message) => console.log(message.sid))
        .done();
    }else if(sensorData > 20) {
        console.log(`Waste bin status : ${sensorData}`);
        sms = 'SMS not send';
    }

  }, function (errorObject) {
    console.log("The read failed: " + errorObject.code);
});

app.get('/', (req, res) => {
  res.send("Waste bin project 2019")
})

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`Example app listening on port ${port}!`));